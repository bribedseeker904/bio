# DAVID STASTNY

## Bio & Contact

```json
{
  "birth": "29.07.1979",
  "phone": "+420 737 540 379",
  "email": "david@davidstastny.cz",
  "place": "Brno, Czech Republic"
}
```

## Work Experience

| Period | Role | Company |
|--------|------|---------|
| 2014 - 2019 | Senior Java Developer, Software Architect | Simplity s.r.o. |
| 2010 - 2014 | Senior Java Developer, Software Architect | Profinit s.r.o. |
| 2009 - 2009 | Lead Developer, Software Architect | Passion Communications s.r.o. |
| 2008 - 2010 | Senior Java Developer, Software Architect | MEDIA SOLUTIONS SLOVAKIA a.s. |
| 2005 - 2008 | Lead Developer, Software Architect | Pears Health Cyber s.r.o. |
| 2003 - 2005 | Lead Developer, Founder | TRITONUS Technologies v.o.s. |
| 2002 - 2003 | Senior Developer | VUMS Legend s.r.o. |
| 2000 - 2002 | Developer | Allied Visions s.r.o. |

## Education

```json
{
  "university": "Faculty of electrotechnics, Czech Technical University, Prague (without degree)",
  "languages": [ "czech (native)", "english" ],
  "trainings": [ "Principal Certified Lotus Notes Developer" ]
}
```

## Project Details

### KYC MIDDLEWARE

![Simplity](./images/simplity.png)

The project is focused on delivery of KYC Middleware for Erste onboarding solution.
The KYC Middleware is a backend application that delivers KYC as a service for the Erste Group, executing DOT (Digital
Onboarding Toolkit) services and running compliance checks.

```json
{
  "client": "Simplity, Erste Group, Slovenska Sporitelna",
  "industry": "finance",
  "duration": { "since": 2018, "until": 2019 },
  "role(s)": [ "Software Architect", "Lead Developer" ],
  "technologies": [ "Kotlin", "Spring Boot", "Event Sourcing", "PostgreSQL", "Amazon S3",
                    "Innovatrics DOT", "Gradle" ]
}
```

### GEORGE PLUS CONNECT

![Simplity](./images/simplity.png)

The project is focused on a delivery of a complete plugin management system used by third parties to register their
plugins into the George+ platform. During a partial assignment, my role was to contribute to both frontend and backend
solutions.

```json
{
  "client": "Simplity, Erste Group",
  "industry": "finance",
  "duration": { "since": 2018, "until": 2018 },
  "role(s)": [ "Senior Developer" ],
  "technologies": [ "React/Redux JS", "Java", "Spring Boot", "REST", "PostgreSQL", "Maven" ]
}
```

### GLOSSARY

![Simplity](./images/simplity.png)

Glossary is part of Accurity Software Suite, a data governance solution. Glossary provides business information
modelling features, technical data mappings, data lineage visualisations, entity relationship diagrams and many other
cool features. 

As the architect of Accurity Software Suite, my responsibilities included defining technologies, processes,
architectural concepts to support implementation of company vision.

As the lead developer, my responsibilities included leading, coaching, mentoring a team of developers, designing and
implementing product features, supporting deployments on-site and refining internal development processes.

```json
{
  "client": "Simplity, Bank of Ireland",
  "industry": "business intelligence",
  "duration": { "since": 2018, "until": 2018 },
  "role(s)": [ "Software Architect", "Lead Developer", "Team Leader" ],
  "technologies": [ "Java", "Spring Boot", "React.JS", "Solr", "Oracle", "Teradata", "MS SQL", "Apache Camel", "Maven" ]
}
```

### QUALITY

![Simplity](./images/simplity.png)

Quality is part of Accurity Software Suite, a data governance solution. Quality provides a measurements engine to
validate business data store in external relational databases, data quality rules, aggregate checks and data integrity
analyses. The dashboards then provide an intuitive overview on results of data quality checks. Quality also supports
integration with ETL solutions like Informatica PowerCenter or CloverETL with the possibility to fail workflow when
data is not meeting configured thresholds.

As the architect of Accurity Software Suite, my responsibilities included defining technologies, processes,
architectural concepts to support implementation of company vision.

As the lead developer, my responsibilities included leading, coaching, mentoring a team of developers, designing and
implementing product features, supporting deployments on-site and refining internal development processes.

```json
{
  "client": "Simplity, J&T Bank",
  "industry": "business intelligence",
  "duration": { "since": 2014, "until": 2017 },
  "role(s)": [ "Software Architect", "Lead Developer", "Team Leader" ],
  "technologies": [ "Java", "Spring Boot", "React.JS", "Solr", "Oracle", "Teradata", "MS SQL",
                    "Apache Camel", "Java Server Faces", "Maven" ]
}
```

### MANTA TOOLS

![Profinit](./images/profinit.png)

Manta Tools is an award-winning software suite helping enterprises to automate data lineage across different
technologies and check and validate SQL scripts and ETL solutions by defining sets of rules.

As the architect and author of Manta Tools, my responsibilities included defining technologies, architecture, user
experience requirements as well as support in pre-sales activities.

As the lead developer, my responsibilities included leading a team of developers, designing and implementing product
features and migrating legacy data lineage and ETL validation solution into Manta for Komercni banka.

```json
{
  "client": "Profinit, Komercni banka",
  "industry": "business intelligence",
  "duration": { "since": 2010, "until": 2014 },
  "role(s)": [ "Software Architect", "Lead Developer", "Team Leader" ],
  "technologies": [ "Java/EE", "Spring Boot", "Apache Commons", "XSLT", "Oracle", "Teradata", "Informatica", "Freemarker" ]
}
```

### CONTRACT FOR MEDIA

![MEDIA SOLUTIONS](./images/media-solutions-slovakia.png)

Contract for Media is a product designed for publishing houses to manage ad campaigns, public events, title distributions
among other features. This product is complemented by Nexus Archive, a multimedia storage with sophisticated searching
features and in-place media editing.

As the architect, my responsibilities included defining architecture, processes and technologies to fulfil company vision.

As the lead developer, my responsibilities included leading a team of developers, designing and implementing product
features while also supporting legacy versions of both products.

```json
{
  "client": "MEDIA SOLUTIONS SLOVAKIA",
  "industry": "publishing/media",
  "duration": { "since": 2008, "until": 2010 },
  "role(s)": [ "Software Architect", "Lead Developer" ],
  "technologies": [ "Java", "Eclipse Rich Client Platform", "Google Web Toolkit", "PostgreSQL", "MySQL", "XSLT",
                    "Eclipse Modelling Framework", "JavaScript" ]
}
```

### E-DETAILING PLATFORM

![Pears Health Cyber](./images/pears-health-cyber.gif)

The e-Detailing platform is a digital solution for marketing and promoting new products by pharmaceutical companies and
targeted at educating pharmaceutical professionals.

As the architect, my responsibilities included defining a flexible architecture to meet various deployment scenarios and
to integrate Flash-based animations with Java application collecting data from users including their behaviour during
these animations.

As the lead developer, my responsibilities included designing and implementing both Java-based flexible and scalable
application platform and Flash-based configurable and extensible runtime environment to easily deploy animations
developed by third-parties.

```json
{
  "client": "Pears Health Cyber",
  "industry": "pharmaceutical",
  "duration": { "since": 2005, "until": 2008 },
  "role(s)": [ "Software Architect", "Lead Developer" ],
  "technologies": [ "Java/EE", "ATG Dynamo", "ATG Scenario Server", "PostgreSQL", "MySQL", "XSLT", "Macromedia Flash",
                    "ActionScript 2.0"]
}
```
